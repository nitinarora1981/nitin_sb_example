# PriceDiff

## Purpose

To get a separate application for handling pricediff functionality,
making it easier for Team Nord to do their work without stepping on the toes
of other teams.

The idea is to have a separate webapp with handling of the pricediff UI and
backing services, as well as having a library containing the main logic of
pricediff (this will be discusses with architect and team Svan first though).

## Tech

The app is using Spring Boot, with Maven plugin for compiling React Javascript UI
using NPM and Node.

We're using Spring Data REST to publish a REST API with HATEOAS support
for CRUD operations against the PriceDiffConfig and PriceDiffSetting entities.

## How to run it

Use Maven or a Maven-capable IDE to build the project.

Run the main application class PriceDiffApplication in your IDE.

An embedded Tomcat server gets started on port 8080.

Then click this link: [Browse the UI](http://localhost:8080/)

You'll get a simple React page with no data in it.

Use the REST API to post data:

For example, POST to http://127.0.0.1:8080/api/config/ with the following data:

{"eventGroupId" :"1",
 "defaultPayback" : "92.00",
 "maxPayback" : "97.00",
 "version" : "1",
 "enabled": "true",
 "defaultOffset" : "0.00"}
 
 .. will add a price diff config entry. Remember to add header 
 "Content-Type" with content "application/json".
 
 ## Questions?
 
 Contact Team Nord in their Hipchat channel.