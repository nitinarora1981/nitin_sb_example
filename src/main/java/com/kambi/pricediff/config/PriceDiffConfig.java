package com.kambi.pricediff.config;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
public class PriceDiffConfig {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer eventGroupId;
    private String market;
    private BigDecimal defaultPayback;
    private BigDecimal defaultOffset;
    private BigDecimal maxPayback;
    private Long version;
    private Boolean enabled;
    // Needed by JPA
    protected PriceDiffConfig() {
    }

    public PriceDiffConfig(String market, BigDecimal defaultPayback, BigDecimal defaultOffset,
                           BigDecimal maxPayback, Long version, Boolean enabled) {
        this.market = market;
        this.defaultPayback = defaultPayback;
        this.defaultOffset = defaultOffset;
        this.maxPayback = maxPayback;
        this.version = version;
        this.enabled = enabled;
    }
}
