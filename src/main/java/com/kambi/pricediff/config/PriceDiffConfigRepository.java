package com.kambi.pricediff.config;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.math.BigDecimal;
import java.util.List;

@RepositoryRestResource(path = "config")
public interface PriceDiffConfigRepository extends JpaRepository<PriceDiffConfig, Integer> {
    List<PriceDiffConfig> findByEventGroupIdAndDefaultOffsetAndVersion(Integer eventGroupId, BigDecimal defaultOffset,
                                                                       Long version);
}
