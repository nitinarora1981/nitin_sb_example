package com.kambi.pricediff.setting;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "setting")
public interface PriceDiffSettingRepository extends JpaRepository<PriceDiffSetting, Integer> {
}
