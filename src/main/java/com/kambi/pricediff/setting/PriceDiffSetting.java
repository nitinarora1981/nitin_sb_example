package com.kambi.pricediff.setting;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
public class PriceDiffSetting {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer customerId;
    private Long eventGroupId;
    private String market;
    private BigDecimal payback;
    private BigDecimal theOffset;
    private Long version;

    // Needed by JPA
    protected PriceDiffSetting() {
    }

    public PriceDiffSetting(Integer customerId,
                            Long eventGroupId,
                            String market,
                            BigDecimal payback,
                            BigDecimal theOffset,
                            Long version) {
        this.customerId = customerId;
        this.eventGroupId = eventGroupId;
        this.market = market;
        this.payback = payback;
        this.theOffset = theOffset;
        this.version = version;
    }
}
