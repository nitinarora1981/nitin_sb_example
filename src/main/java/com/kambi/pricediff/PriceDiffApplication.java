package com.kambi.pricediff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.kambi.pricediff"})
public class PriceDiffApplication {
    public static void main(String[] args) {
        SpringApplication.run(PriceDiffApplication.class, args);
    }
}
