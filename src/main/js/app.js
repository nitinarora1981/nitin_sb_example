'use strict';

// tag::vars[]
const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
// end::vars[]

// tag::app[]
class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {configs: []};
    }

    componentDidMount() {
        client({method: 'GET', path: '/api/config'}).done(response => {
            this.setState({configs: response.entity._embedded.priceDiffConfigs});
        });
    }

    render() {
        return (
            <ConfigList configs={this.state.configs}/>
        )
    }
}
// end::app[]

// tag::config-list[]
class ConfigList extends React.Component{
    render() {
        var configs = this.props.configs.map(config =>
            <Config key={config._links.self.href} config={config}/>
        );
        return (
            <table>
                <tbody>
                <tr>
                    <th>Event Group ID</th>
                    <th>Default Payback</th>
                    <th>Max Payback</th>
                </tr>
                {configs}
                </tbody>
            </table>
        )
    }
}
// end::config-list[]

// tag::config[]
class Config extends React.Component{
    render() {
        return (
            <tr>
                <td>{this.props.config.eventGroupId}</td>
                <td>{this.props.config.defaultPayback}</td>
                <td>{this.props.config.maxPayback}</td>
            </tr>
        )
    }
}
// end::config[]

// tag::render[]
ReactDOM.render(
    <App />,
    document.getElementById('react')
)
// end::render[]

