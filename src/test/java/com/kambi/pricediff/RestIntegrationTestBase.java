package com.kambi.pricediff;

import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {PriceDiffApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class RestIntegrationTestBase {
    @LocalServerPort
    protected Integer port;

    @Value("${spring.data.rest.base-path}")
    protected String basePath;

    @Before
    public void setUp() {
        RestAssured.port = port;
        RestAssured.basePath = basePath;
    }
}
