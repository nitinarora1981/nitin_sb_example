package com.kambi.pricediff.ping;

import com.kambi.pricediff.RestIntegrationTestBase;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class PingControllerIT extends RestIntegrationTestBase {
    @Test
    public void pingReturnsPong() {
        given().
        when().
            get("ping").
        then().
            statusCode(200).
            body(equalTo("PONG"));
    }
}