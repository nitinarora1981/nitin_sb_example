package com.kambi.pricediff.config;

import com.kambi.pricediff.RestIntegrationTestBase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PriceDiffConfigRepositoryIT extends RestIntegrationTestBase {

    @Autowired
    private PriceDiffConfigRepository repository;

    @Test
    public void canCreatePriceDiffConfig() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("eventGroupId", "1");
        json.put("defaultPayback", new BigDecimal("92.00"));
        json.put("maxPayback", new BigDecimal("97.00"));
        json.put("version", 1L);
        json.put("enabled", true);
        BigDecimal defaultOffset = new BigDecimal("0.00");
        json.put("defaultOffset", defaultOffset);

        long count = repository.count();
        assertThat(count, is(0L));

        given().
            header("Content-Type", "application/json").
            body(json.toString()).
        when().
            post("config").
        then().
            statusCode(201);

        count = repository.count();
        assertThat(count, is(1L));

        PriceDiffConfig priceDiffConfig = repository.findAll().get(0);
        assertThat(priceDiffConfig.getDefaultOffset(), is(defaultOffset));

        List<PriceDiffConfig> priceDiffConfigs = repository.findByEventGroupIdAndDefaultOffsetAndVersion(1, new BigDecimal("0.00"), 1L);
        priceDiffConfig = priceDiffConfigs.get(0);
        assertThat(priceDiffConfig.getDefaultOffset(), is(defaultOffset));
    }
}
