package com.kambi.pricediff.setting;

import com.kambi.pricediff.RestIntegrationTestBase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PriceDiffSettingRepositoryIT extends RestIntegrationTestBase {
    @Autowired
    private PriceDiffSettingRepository repository;

    @Test
    public void canCreatePriceDiffSetting() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("eventGroupId", "1");
        json.put("customerId", "1");
        json.put("payback", new BigDecimal("92.00"));
        BigDecimal offset = new BigDecimal("5.00");
        json.put("theOffset", offset);
        json.put("market", "se");
        json.put("version", 1L);

        long count = repository.count();
        assertThat(count, is(0L));

        given().
            header("Content-Type", "application/json").
            body(json.toString()).
        when().
            post("setting").
        then().
            statusCode(201);

        count = repository.count();
        assertThat(count, is(1L));

        PriceDiffSetting priceDiffSetting = repository.findAll().get(0);
        assertThat(priceDiffSetting.getTheOffset(), is(offset));
    }
}
